import Vue from 'vue';
import Quasar, * as All from 'quasar-framework';

Vue.use(Quasar, {
  components: All,
  directives: All,
});

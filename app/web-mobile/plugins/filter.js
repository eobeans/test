import Vue from 'vue';

const filters = {
  fSex(text) {
    switch (parseInt(text)) {
      case 0: return '男';
      case 1: return '女';
      default: '';
    }
  },
  fReferType(text) {
    switch (parseInt(text)) {
      case 1: return '转门诊';
      case 2: return '转检查';
      case 3: return '转住院';
      case 4: return '转康复';
      default: '';
    }
  },
  fReferStatus(text) {
    switch (parseInt(text)) {
      case 1: return '待提交';
      case 2: return '待审核';
      case 3: return '待接收';
      case 4: return '已拒绝';
      case 5: return '已取消';
      case 6: return '已接收';
      case 7: return '已过期';
      case 99: return '已结束';
      default: '';
    }
  },
  fReferOption(text, mode) {
    if (!text) return '';
    switch (parseInt(text)) {
      case 1: return '修改';
      case 2: if (parseInt(mode) !== 0) {
        return '审核';
      } return '撤回';
      case 3: if (parseInt(mode) !== 0) {
        return '报到';
      } return '转诊单';
      case 4: return '转诊单';
      case 5: return '转诊单';
      case 6: return '转诊单';
      case 7: return '转诊单';
      case 99: return '转诊单';
      default: '';
    }
  },
  fReferColor(text) {
    switch (parseInt(text)) {
      case 1: return 'faded';
      case 2: return 'warning';
      case 3: return 'warning';
      case 4: return 'negative';
      case 5: return 'negative';
      case 6: return 'positive';
      case 7: return 'faded';
      case 99: return 'faded';
      default: '';
    }
  },
  fConsultOption(text, mode) {
    switch (parseInt(text)) {
      case 1: return '修改';
      case 2: if (parseInt(mode) !== 0) {
        return '审核';
      } return '撤回';
      case 3: return '开始会诊';
      case 4: return '报告';
      case 5: return '报告';
      case 6: return '报告';
      case 99: return '报告';
      default: '';
    }
  },
  fConsultColor(text) {
    switch (parseInt(text)) {
      case 1: return 'faded';
      case 2: return 'warning';
      case 3: return 'positive';
      case 4: return 'negative';
      case 5: return 'faded';
      case 6: return 'faded';
      case 99: return 'faded';
      default: '';
    }
  },
  fConsultStatus(text) {
    switch (parseInt(text)) {
      case 1: return '待提交';
      case 2: return '待审核';
      case 3: return '待会诊';
      case 4: return '已拒绝';
      case 5: return '已取消';
      case 6: return '已过期';
      case 99: return '已结束';
      default: '';
    }
  },
  fConsultType(text) {
    switch (parseInt(text)) {
      case 0: return '临床会诊';
      case 1: return '影像会诊';
      case 2: return '心电会诊';
      case 3: return '病理会诊';
      default: '';
    }
  },
  fAffirmStatus(text) {
    switch (parseInt(text)) {
      case 1: return '无需审核';
      case 2: return '待审核';
      case 3: return '审核通过';
      case 4: return '审核不通过';
      default :return '';
    }
  },
  fConsultMode(text) {
    switch (parseInt(text)) {
      case 0: return '离线会诊';
      case 1: return '在线会诊';
      default :return '离线会诊';
    }
  },
  fStaffList(staffList) {
    let stuffName = '';
    if (staffList) {
      staffList.forEach(e => { stuffName = stuffName + e.serveDoctorName + ' '; });
    }
    return stuffName;
  },
  fExamStatus(text) {
    switch (parseInt(text)) {
      // case 0: return '取消检查';
      // case 10: return '收到申请';
      // case 20: return '申请确认';
      // case 30: return '正在检查';
      // case 40: return '检查完毕';
      // case 50: return '收到影像';
      // case 56: return '待写报告';
      // case 58: return '尚未提交';
      // case 60: return '初步报告';
      // case 65: return '审核退回';
      // case 70: return '报告完成';
      case 56: return '待诊断';
      case 60: return '待确认';
      case 70: return '已确认';
      default: return '其他';
    }
  },
  fHospitalLevel(text) {
    switch (parseInt(text)) {
      case 0: return '三级甲';
      case 1: return '三级乙';
      case 2: return '三级丙';
      case 3: return '二级甲';
      case 4: return '二级乙';
      case 5: return '二级丙';
      case 6: return '一级甲';
      case 7: return '一级乙';
      case 8: return '一级丙';
      default: '';
    }
  },
  fJobLevel(text) {
    if (!text) return '其他';
    switch (parseInt(text)) {
      case 1: return '住院医师';
      case 2: return '主治医师';
      case 3: return '副主任医师';
      case 4: return '主任医师';
      case 99: return '其他';
      default : return '其他';
    }
  },
  fPatientSource(text) {
    if (!text) return '其他';
    switch (parseInt(text)) {
      case 1: return '住院';
      case 2: return '门诊';
      case 3: return '体检';
      case 4: return '其他';
      default : return '其他';
    }
  },
  fRadiologyColor(text) {
    switch (parseInt(text)) {
      case 0: return 'faded';
      case 10: return 'warning';
      case 30: return 'warning';
      case 20: return 'positive';
      case 40: return 'positive';
      case 50: return 'warning';
      case 56: return 'warning';
      case 58: return 'warning';
      case 60: return 'positive';
      case 65: return 'warning';
      case 70: return 'faded';
      default: return '';
    }
  },
  fRadiologyMethod(text) {
    switch (parseInt(text)) {
      case 56: return '编写';
      case 60: return '审核';
      case 70: return '报告';
      default: return '报告';
    }
  },
  host(url) {
    if (!url) return '';
    const host = url.replace(/^https?:\/\//, '').replace(/\/.*$/, '');
    const parts = host.split('.').slice(-3);
    if (parts[0] === 'www') parts.shift();
    return parts.join('.');
  },
  slice(text, length) {
    if (!text) return '';
    return text.length > length ? text.slice(0, length) + '...' : text;
  },
  unescape(text) {
    let res = text || ''

    ;[
      [ '<p>', '\n' ],
      [ '&amp;', '&' ],
      [ '&amp;', '&' ],
      [ '&apos;', '\'' ],
      [ '&#x27;', '\'' ],
      [ '&#x2F;', '/' ],
      [ '&#39;', '\'' ],
      [ '&#47;', '/' ],
      [ '&lt;', '<' ],
      [ '&gt;', '>' ],
      [ '&nbsp;', ' ' ],
      [ '&quot;', '"' ],
    ].forEach(pair => {
      res = res.replace(new RegExp(pair[0], 'ig'), pair[1]);
    });
    return res;
  },
};

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key]);
});

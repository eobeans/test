const host = process.env.PROXY_REWRITE;
const $C = {
  imagePath: host,
  host,
  loginPath: '/login',
  adminLoginPath: '/customer/login',
  baseSys: 'http://120.27.129.243:8090/BaseSys/',
  medSys: 'http://120.27.129.243:8088/GradeMedMobile/',
  apiPath: process.env.API_HOST ? process.env.API_HOST : host,
  deviceId: process.env.DEVICE_ID,
  pageSize: 10, // 分页每页数量
  monthNames: [
    '1月',
    '2月',
    '3月',
    '4月',
    '5月',
    '6月',
    '7月',
    '8月',
    '9月',
    '10月',
    '11月',
    '12月',
  ],
  dayNames: [
    '一',
    '二',
    '三',
    '四',
    '五',
    '六',
    '日',
  ],
};
export default $C;

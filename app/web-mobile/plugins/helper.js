import qs from 'qs';
import axios from 'axios';
import moment from 'moment';
const helper = {};

function escapeCookie(value) {
  return String(value).replace(/[,;"\\=\s%]/g, function(character) {
    return encodeURIComponent(character);
  });
}

helper.setCookie = function(key, value, time) {
  time = time ? time : 1000 * 60 * 60 * 24;
  const date = new Date();// 将date设置为1天以后的时间
  date.setTime(date.getTime() + time);
  document.cookie = escapeCookie(key) + '=' + escapeCookie(value) + ';expires=' + date.toGMTString() + ';path=/';
};

helper.getCookie = function(name) {
  const m = String(document.cookie).match(new RegExp('(?:^| )' + name + '(?:(?:=([^;]*))|;|$)')) || [ '', '' ];
  return decodeURIComponent(m[1]);
};
// 获取http入参
helper.getQueryString = function(name) {
  const reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
  const searches = location.href.split('?');
  const r = searches[1] ? searches[1].match(reg) : searches[1];
  if (r != null) return decodeURIComponent(r[2]); return null;
};

helper.GET = async function(url, data, option) {
  try {
    if (!option) { option = {}; }
    if (data) { option.params = data; }
    const response = await axios.get(url, option);
    return JSON.parse(response.data);
  } catch (e) {
    return e;
  }
};

helper.POST = async function(url, data, option) {
  try {
    if (!option) { option = {}; }
    if (data) { data = qs.stringify(data); }
    const response = await axios.post(url, data, option);
    return JSON.parse(response.data);
  } catch (e) {
    return e;
  }
};

helper.pickDatetime = async function(type, datetime) {
  return await new Promise(function(resolve) {
    if (!datetime) { datetime = new Date(); }
    if (global.cordova && global.cordova.plugins.DateTimePicker) {
      global.cordova.plugins.DateTimePicker.show({
        mode: type ? type : 'date',
        date: moment(datetime).toDate(),
        locale: 'zh_CN',
        okText: '选择',
        cancelText: '取消',
        success(newDate) {
          console.log(newDate);
          resolve(newDate);
        },
        cancel() {
          resolve(datetime);
        },
        error(err) {
          console.log(err);
          resolve(datetime);
        },
      });
    } else {
      resolve(datetime);
    }
  });
};

helper.startConsult = function(userId, groupId) {
  if (global.anychat) {
    global.anychat.call(userId, groupId);
  }
};

export default helper;

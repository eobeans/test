import $C from '../plugins/$C';
import helper from '../plugins/helper';
import moment from 'moment';
import R from 'ramda';

export const state = () => ({
  referralDetail: {
    referId: '',
    referType: 1,
    referReason: '',
    referStatus: 1,
    patientSummary: '',
    referDemand: '',
  },
  hospital: {
    receiveArea: '',
    receiveHospital: '', // 转入医院编号
    receiveHospitalName: '',
    receiveDept: '',
    receiveDeptName: '',
  },
  doctor: {
    receiveDoctor: '',
    receiveDoctorName: '',
    receiveDoctorPhone: '',
  },
  patient: {
    hospitalPatientId: '',
    sickId: '',
    name: '',
    sex: '',
    age: '',
  },
  schedule: {
    scheduleDate: '',
    scheduleTime: '',
    scheduleApm: '',
  },
  attachments: [],
});

export const mutations = {
  updateReferral(state, referral) {
    state.referralDetail.referId = referral.referId;
    state.referralDetail.referType = referral.referType;
    state.referralDetail.referReason = referral.referReason;
    state.referralDetail.referStatus = referral.referStatus;
    state.referralDetail.patientSummary = referral.patientSummary;
    state.referralDetail.referDemand = referral.referDemand;
    state.hospital.receiveArea = referral.receiveArea;
    state.hospital.receiveHospital = referral.receiveHospital;
    state.hospital.receiveHospitalName = referral.receiveHospitalName;
    state.hospital.receiveDept = referral.receiveDept;
    state.hospital.receiveDeptName = referral.receiveDeptName;
    state.doctor.receiveDoctor = referral.receiveDoctor;
    state.doctor.receiveDoctorName = referral.receiveDoctorName;
    state.doctor.receiveDoctorPhone = referral.receiveDoctorPhone;
    state.patient.hospitalPatientId = referral.sickId;
    state.patient.sickId = referral.sickId;
    state.patient.name = referral.name;
    state.patient.sex = referral.sex;
    state.patient.age = referral.age;
    state.schedule.scheduleDate = referral.scheduleDate;
    state.schedule.scheduleTime = referral.scheduleTime;
    state.schedule.scheduleApm = referral.scheduleApm;
    state.attachments = [];
  },
  updateReferralDetail(state, referral) {
    state.referralDetail.referId = referral.referId;
    state.referralDetail.referType = referral.referType;
    state.referralDetail.referReason = referral.referReason;
    state.referralDetail.referStatus = referral.referStatus;
    state.referralDetail.patientSummary = referral.patientSummary;
    state.referralDetail.referDemand = referral.referDemand;
  },
  updateReferralHospital(state, hospital) {
    state.hospital.receiveArea = hospital.areaCode;
    state.hospital.receiveHospital = hospital.hospitalId;
    state.hospital.receiveHospitalName = hospital.name;
    state.hospital.receiveDept = hospital.deptId;
    state.hospital.receiveDeptName = hospital.deptName;
  },
  updateReferralDoctor(state, doctor) {
    state.doctor.receiveDoctor = doctor.doctorId;
    state.doctor.receiveDoctorName = doctor.name;
    state.doctor.receiveDoctorPhone = doctor.doctorPhone;
  },
  updateReferralPatient(state, patient) {
    state.patient.hospitalPatient_id = patient.mpiId;
    state.patient.sickId = patient.mpiId;
    state.patient.name = patient.name;
    state.patient.sex = patient.sexCode;
    state.patient.age = patient.birthDate ? moment().diff(moment(patient.birthDate), 'years') : 0;
  },
  updateReferralSchedule(state, schedule) {
    state.schedule.scheduleDate = schedule.sourceDate;
    state.schedule.scheduleTime = schedule.timeBegin;
    state.schedule.scheduleApm = schedule.sourceApm;
  },
  updateReferDemand(state, referDemand) {
    state.referralDetail.referDemand = referDemand;
  },
  updateReferReason(state, referReason) {
    state.referralDetail.referReason = referReason;
  },
  updatePatientSummary(state, patientSummary) {
    state.referralDetail.patientSummary = patientSummary;
  },
  updateAttachments(state, attachments) {
    state.attachments = attachments;
  },
  resetReferral(state) {
    state.referralDetail = {
      referId: '',
      referType: 1,
      referReason: '',
      referStatus: 1,
      patientSummary: '',
      referDemand: '',
    };
    state.hospital = {
      receiveArea: '',
      receiveHospital: '', // 转入医院编号
      receiveHospitalName: '',
      receiveDept: '',
      receiveDeptName: '',
    };
    state.doctor = {
      receiveDoctor: '',
      receiveDoctorName: '',
      receiveDoctorPhone: '',
    };
    state.patient = {
      outpatientNo: '',
      inpatientNo: '',
      hospitalPatient_id: '',
      sickId: '',
      name: '',
      sex: '',
      age: '',
    };
    state.schedule = {
      scheduleDate: '',
      scheduleTime: '',
      scheduleApm: '',
    };
    state.attachments = [];
  },
  resetReferralDetail(state) {
    state.referralDetail = {
      referId: '',
      referType: 1,
      referReason: '',
      referStatus: 1,
      patientSummary: '',
      referDemand: '',
    };
  },
  resetReferralHospital(state) {
    state.hospital = {
      receiveArea: '',
      receiveHospital: '', // 转入医院编号
      receiveHospital_name: '',
      receiveDept: '',
      receiveDeptName: '',
    };
  },
  resetReferralDoctor(state) {
    state.doctor = {
      receiveDoctor: '',
      receiveDoctorName: '',
      receiveDoctorPhone: '',
    };
  },
  resetReferralPatient(state) {
    state.patient = {
      outpatientNo: '',
      inpatientNo: '',
      hospitalPatient_id: '',
      sickId: '',
      name: '',
      sex: '',
      age: '',
    };
  },
  resetReferralSchedule(state) {
    state.schedule = {
      scheduleDate: '',
      scheduleTime: '',
      scheduleApm: '',
    };
  },
};

export const actions = {
  async changeStatus({ commit, rootState, state }, status) {
    const res = await helper.POST($C.medSys + 'v1/referral/referInfo/update', {
      token: rootState.corsuser.user.token,
      referId: state.referralDetail.referId,
      referStatus: status,
    });
    const referralDetail = R.clone(state.referralDetail);
    referralDetail.referStatus = status;
    if (!parseInt(res.status) && res.result && res.result.length > 0) {
      commit('updateReferralDetail', referralDetail);
    } else {
      throw new Error(res.message);
    }
  },
  async submit({ rootState, state }) {
    const params = R.mergeAll([{
      token: rootState.corsuser.user.token,
    },
    state.referralDetail,
    state.hospital,
    state.doctor,
    state.patient,
    state.schedule,
    ]);
    if (!params.referId) {
      params.referId = '' + (new Date()).getTime() + parseInt(Math.random() * 100000);
      const res = await helper.POST($C.medSys + 'v1/referral/referInfo/add', params);
      if (parseInt(res.status)) {
        throw new Error(res.message);
      }
    } else {
      if (parseInt(params.referStatus) === 1) {
        params.referStatus = 2;
      }
      const res = await helper.POST($C.medSys + 'v1/referral/referInfo/update', params);
      if (parseInt(res.status)) {
        throw new Error(res.message);
      }
    }
  },
  async changeType({ commit, state }, type) {
    const referralDetail = R.clone(state.referralDetail);
    referralDetail.referType = type;
    commit('updateReferralDetail', referralDetail);
  },
  async acquireAttachments({ commit, state, rootState }) {
    const axios = require('axios');
    const qs = require('qs');
    const params = {
      token: rootState.corsuser.user.token,
      eventType: 0,
      eventId: state.referralDetail.referId,
    };
    const res = await helper.POST($C.medSys + 'v1/common/file/list', params);
    const attachments = [];
    if (!parseInt(res.status)) {
      if (res.result.length > 0) {
        const promises = res.result[0].ermFileList.map(async el => {
          const result = await axios.post($C.medSys + 'v1/common/file/getFileInfo', qs.stringify({
            token: rootState.corsuser.user.token,
            fileNo: el.fileNo,
          }), { responseType: 'arraybuffer' });
          if (!result.data.status) {
            attachments.push({
              fileNo: el.fileNo,
              fileName: el.fileName,
              fileType: el.fileType,
              fileFormat: el.fileFormat,
              status: el.status,
              src: 'data:image/png;base64, ' + new Buffer(result.data, 'binary').toString('base64'),
            });
          }
        });
        await Promise.all(promises);
        commit('updateAttachments', attachments);
      }
    }
  },
};

import $C from '../plugins/$C';
import helper from '../plugins/helper';

export const state = () => ({
  referralOutList: [],
});

export const mutations = {
  update(state, referralOutList) {
    state.referralOutList = referralOutList;
  },
  clean(state) {
    state.referralOutList = [];
  },
};

export const actions = {
  async refresh({ commit, rootState }) {
    try {
      const res = await helper.POST($C.medSys + 'v1/referral/referOutInfo/list', {
        token: rootState.corsuser.user.token,
        pageNo: '1',
        pageSize: $C.pageSize,
      });
      if (!parseInt(res.status)) {
        commit('update', res.result);
      }
    } catch (e) {
      console.log(e);
    }
  },
  async loadMore({ commit, rootState, state }, index) {
    try {
      const res = await helper.POST($C.medSys + 'v1/referral/referOutInfo/list', {
        token: rootState.corsuser.user.token,
        pageNo: index + 1,
        pageSize: $C.pageSize,
      });
      if (!parseInt(res.status)) {
        commit('update', state.referralOutList.concat(res.result));
      }
    } catch (e) {
      console.log(e);
    }
  },
};

import $C from '../plugins/$C';
import helper from '../plugins/helper';

export const state = () => ({
  noticeList: [],
});

export const mutations = {
  update(state, noticeList) {
    state.noticeList = noticeList;
  },
  clean(state) {
    state.noticeList = [];
  },
};

export const actions = {
  async refresh({ commit, rootState }) {
    const res = await helper.POST($C.medSys + 'v1/common/article/noticeList', {
      token: rootState.corsuser.user.token,
      pageNo: '1',
      pageSize: $C.pageSize,
    });
    if (!parseInt(res.status)) {
      commit('update', res.result);
    } else {
      throw new Error(res.message);
    }
  },
  async loadMore({ commit, rootState, state }, index) {
    try {
      const res = await helper.POST($C.medSys + 'v1/common/article/noticeList', {
        token: rootState.corsuser.user.token,
        pageNo: index + 1,
        pageSize: $C.pageSize,
      });
      if (!parseInt(res.status)) {
        commit('update', state.noticeList.concat(res.result));
      }
    } catch (e) {
      console.log(e);
    }
  },
};

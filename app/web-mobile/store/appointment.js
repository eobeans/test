import $C from '../plugins/$C';
import helper from '../plugins/helper';
import moment from 'moment';

export const state = () => ({
  appointmentList: [],
});

export const mutations = {
  update(state, appointmentList) {
    state.appointmentList = appointmentList;
  },
  clean(state) {
    state.appointmentList = [];
  },
};

export const actions = {
  async refresh({ commit, state, rootState }, doctorCode) {
    commit('clean');
    try {
      for (let num = 0; num < 8; num++) {
        const sourceDate = moment().add(num, 'days').format('YYYY-MM-DD');
        const res = await helper.POST($C.medSys + 'v1/referral/referSource/listByDoctorAndDate', {
          token: rootState.corsuser.user.token,
          sourceDate,
          doctorCode,
        });
        if (!parseInt(res.status)) {
          commit('update', state.appointmentList.concat({ sourceDate, schedule: res.result }));
        }
      }
    } catch (e) {
      console.log(e);
    }
  },
};

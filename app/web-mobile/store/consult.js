import $C from '../plugins/$C';
import helper from '../plugins/helper';
import moment from 'moment';
import R from 'ramda';

export const state = () => ({
  consultDetail: {
    consultId: '',
    consultMode: 1,
    scheduledDate: '',
    scheduledTime: '',
    firstVisit: '',
    consultPurpose: '',
    diseaseSummary: '',
    consultStatus: 1,
  },
  staffList: [],
  patient: {
    hospitalPatientId: '',
    sickId: '',
    name: '',
    sex: '',
    age: '',
  },
  extend: {
    patientSource: '',
    reqHospitalName: '',
    reqDoctorName: '',
    groupId: '1',
  },
  attachments: [],
});

export const mutations = {
  updateConsult(state, consult) {
    state.consultDetail.consultId = consult.consultId;
    state.consultDetail.consultMode = consult.consultMode;
    state.consultDetail.scheduledDate = consult.scheduledDate;
    state.consultDetail.scheduledTime = consult.scheduledTime;
    state.consultDetail.firstVisit = consult.firstVisit;
    state.consultDetail.consultPurpose = consult.consultPurpose;
    state.consultDetail.diseaseSummary = consult.diseaseSummary;
    state.consultDetail.consultStatus = consult.consultStatus;
    state.patient.hospitalPatientId = consult.sickId;
    state.patient.sickId = consult.sickId;
    state.patient.name = consult.patientName;
    state.patient.sex = consult.patientSex;
    state.patient.age = consult.patientAge;
    state.extend.patientSource = consult.patientSource;
    state.extend.reqHospitalName = consult.reqHospitalName;
    state.extend.reqDoctorName = consult.reqDoctorName;
    state.extend.groupId = consult.groupId;
    state.staffList = consult.staffList;
    state.attachments = [];
  },
  updateConsultDetail(state, consult) {
    state.consultDetail.consultId = consult.consultId;
    state.consultDetail.consultMode = consult.consultMode;
    state.consultDetail.scheduledDate = consult.scheduledDate;
    state.consultDetail.scheduledTime = consult.scheduledTime;
    state.consultDetail.firstVisit = consult.firstVisit;
    state.consultDetail.consultPurpose = consult.consultPurpose;
    state.consultDetail.diseaseSummary = consult.diseaseSummary;
    state.consultDetail.consultStatus = consult.consultStatus;
  },
  updateConsultPatient(state, patient) {
    state.patient.hospitalPatient_id = patient.mpiId;
    state.patient.sickId = patient.mpiId;
    state.patient.name = patient.name;
    state.patient.sex = patient.sexCode;
    state.patient.age = patient.birthDate ? moment().diff(moment(patient.birthDate), 'years') : 0;
  },
  updateConsultStaffList(state, staffList) {
    state.staffList = staffList;
  },
  updateConsultPurpose(state, consultPurpose) {
    state.consultDetail.consultPurpose = consultPurpose;
  },
  updateFirstVisit(state, firstVisit) {
    state.consultDetail.firstVisit = firstVisit;
  },
  updateDiseaseSummary(state, diseaseSummary) {
    state.consultDetail.diseaseSummary = diseaseSummary;
  },
  updateScheduledDate(state, scheduledDate) {
    state.consultDetail.scheduledDate = scheduledDate;
  },
  updateScheduledTime(state, scheduledTime) {
    state.consultDetail.scheduledTime = scheduledTime;
  },
  updateAttachments(state, attachments) {
    state.attachments = attachments;
  },
  resetConsult(state) {
    state.consultDetail = {
      consultId: '',
      consultMode: 1,
      scheduledDate: '',
      scheduledTime: '',
      firstVisit: '',
      consultPurpose: '',
      consultStatus: 1,
      diseaseSummary: '',
    };
    state.staffList = [];
    state.patient = {
      outpatientNo: '',
      inpatientNo: '',
      hospitalPatient_id: '',
      sickId: '',
      name: '',
      sex: '',
      age: '',
    };
    state.extend = {
      patientSource: '',
      reqHospitalName: '',
      reqDoctorName: '',
      groupId: '',
    };
    state.attachments = [];
  },
  resetConsultDetail(state) {
    state.consultDetail = {
      consultId: '',
      consultMode: 1,
      consultStatus: 1,
      scheduledDate: '',
      scheduledTime: '',
      firstVisit: '',
      consultPurpose: '',
      diseaseSummary: '',
    };
  },
  resetStaffList(state) {
    state.staffList = [];
  },
  resetConsultPatient(state) {
    state.patient = {
      outpatientNo: '',
      inpatientNo: '',
      hospitalPatient_id: '',
      sickId: '',
      name: '',
      sex: '',
      age: '',
    };
  },
};

export const actions = {
  async changeStatus({ commit, rootState, state }, status) {
    const res = await helper.POST($C.medSys + 'v1/consult/consultInfo/update', {
      token: rootState.corsuser.user.token,
      consultId: state.consultDetail.consultId,
      sickId: state.patient.sickId,
      consultStatus: status,
    });
    const consultDetail = R.clone(state.consultDetail);
    consultDetail.consultStatus = status;
    if (!parseInt(res.status) && res.result && res.result.length > 0) {
      commit('updateConsultDetail', consultDetail);
    } else {
      throw new Error(res.message);
    }
  },
  async changeAffirmStatus({ rootState, state }, affirmStatus) {
    const res = await helper.POST($C.medSys + 'v1/consult/consultInfo/audit', {
      token: rootState.corsuser.user.token,
      consultId: state.consultDetail.consultId,
      affirmStatus,
    });
    if (parseInt(res.status)) {
      throw new Error(res.message);
    }
  },
  async submit({ rootState, state }) {
    let doctorIds = '';
    state.staffList.forEach(e => { doctorIds = doctorIds + e.serveDoctor + ','; });
    let doctorNames = '';
    state.staffList.forEach(e => { doctorNames = doctorNames + e.serveDoctorName + ','; });
    const params = R.mergeAll([{
      token: rootState.corsuser.user.token,
      doctorIds,
      doctorNames,
    },
    state.consultDetail,
    state.patient,
    ]);
    if (!params.consultId) {
      params.consultId = '' + (new Date()).getTime() + parseInt(Math.random() * 100000);
      const res = await helper.POST($C.medSys + 'v1/consult/consultInfo/add', params);
      if (parseInt(res.status)) {
        throw new Error(res.message);
      }
    } else {
      if (parseInt(params.consultStatus) === 1) {
        params.consultStatus = 2;
      }
      const res = await helper.POST($C.medSys + 'v1/consult/consultInfo/update', params);
      if (parseInt(res.status)) {
        throw new Error(res.message);
      }
    }
  },
  async changeMode({ commit, state }, mode) {
    const consultDetail = R.clone(state.consultDetail);
    consultDetail.consultMode = mode;
    commit('updateConsultDetail', consultDetail);
  },
  async addStaff({ commit, state }, staff) {
    const exist = R.find(R.propEq('serveDoctor', staff.serveDoctor))(state.staffList);
    if (!exist) {
      commit('updateConsultStaffList', state.staffList.concat(staff));
    }
  },
  async removeStaff({ commit, state }, serveDoctor) {
    const staffList = R.reject(R.propEq('serveDoctor', serveDoctor))(state.staffList);
    commit('updateConsultStaffList', staffList);
  },
  async removeAllStaff({ commit }) {
    const staffList = [];
    commit('updateConsultStaffList', staffList);
  },
  async acquireAttachments({ commit, state, rootState }) {
    const axios = require('axios');
    const qs = require('qs');
    const params = {
      token: rootState.corsuser.user.token,
      eventType: 1,
      eventId: state.consultDetail.consultId,
    };
    const res = await helper.POST($C.medSys + 'v1/common/file/list', params);
    const attachments = [];
    if (!parseInt(res.status)) {
      if (res.result.length > 0) {
        const promises = res.result[0].ermFileList.map(async el => {
          const result = await axios.post($C.medSys + 'v1/common/file/getFileInfo', qs.stringify({
            token: rootState.corsuser.user.token,
            fileNo: el.fileNo,
          }), { responseType: 'arraybuffer' });
          if (!result.data.status) {
            attachments.push({
              fileNo: el.fileNo,
              fileName: el.fileName,
              fileType: el.fileType,
              fileFormat: el.fileFormat,
              status: el.status,
              // src: 'data:image/png;base64, ' + new Buffer(result.data, 'binary').toString('base64'),
              src: '/cn/_nuxt/img/banner.4b512cb.png',
            });
          }
        });
        await Promise.all(promises);
        commit('updateAttachments', attachments);
      }
    }
  },
};

import $C from '../plugins/$C';
import helper from '../plugins/helper';

export const state = () => ({
  description: '',
  impression: '',
  reportDoctorName: '',
  reportDate: '',
  auditDoctorName: '',
  reportStatus: 56,
  examNo: '',
  dicomPath: '',
});

export const mutations = {
  update(state, radiologyReport) {
    state.description = radiologyReport.description;
    state.impression = radiologyReport.impression;
    state.reportDoctorName = radiologyReport.reportDoctorName;
    state.reportDate = radiologyReport.reportDate;
    state.auditDoctorName = radiologyReport.auditDoctorName;
    state.reportStatus = radiologyReport.reportStatus;
    state.examNo = radiologyReport.examNo;
    state.dicomPath = radiologyReport.dicomPath;
  },
  updateDescription(state, description) {
    state.description = description;
  },
  updateImpression(state, impression) {
    state.impression = impression;
  },
  updateDicomPath(state, dicomPath) {
    state.dicomPath = dicomPath;
  },
  clean(state) {
    const radiologyReport = {
      description: '',
      impression: '',
      reportDoctorName: '',
      reportDate: '',
      auditDoctorName: '',
      reportStatus: 56,
      examNo: '',
      dicomPath: '',
    };
    state.description = radiologyReport.description;
    state.impression = radiologyReport.impression;
    state.reportDoctorName = radiologyReport.reportDoctorName;
    state.reportDate = radiologyReport.reportDate;
    state.auditDoctorName = radiologyReport.auditDoctorName;
    state.reportStatus = radiologyReport.reportStatus;
    state.examNo = radiologyReport.examNo;
    state.dicomPath = radiologyReport.dicomPath;
  },
};

export const actions = {
  async replace({ commit, rootState }, examNo) {
    try {
      const res = await helper.POST($C.medSys + 'v1/diagnose/examRpt/info', {
        token: rootState.corsuser.user.token,
        examNo,
      });
      if (!parseInt(res.status) && res.result && res.result.length > 0) {
        commit('update', res.result[0].examRpt);
      } else {
        commit('update', {
          description: '',
          impression: '',
          reportDoctorName: '',
          reportDate: '',
          auditDoctorName: '',
          reportStatus: 56,
          examNo,
        });
      }
    } catch (e) {
      console.log(e);
    }
  },
  async submit({ rootState, state }) {
    const res = await helper.POST($C.medSys + 'v1/diagnose/examRpt/update', {
      token: rootState.corsuser.user.token,
      examNo: state.examNo,
      reportStatus: (parseInt(state.reportStatus) === 56) ? 60 : 70,
      impression: state.impression,
      description: state.description,
    });
    if (parseInt(res.status)) {
      throw new Error(res.message);
    }
  },
};

import $C from '../plugins/$C';
import helper from '../plugins/helper';

export const state = () => ({
  doctorList: [],
  friendList: [],
});

export const mutations = {
  updateDoctor(state, doctorList) {
    state.doctorList = doctorList;
  },
  updateFriend(state, friendList) {
    state.friendList = friendList;
  },
  cleanDoctor(state) {
    state.doctorList = [];
  },
  cleanFriend(state) {
    state.friendList = [];
  },
  clean(state) {
    state.doctorList = [];
    state.friendList = [];
  },
};

export const actions = {
  async refresh({ commit, rootState }) {
    const res = await helper.POST($C.medSys + 'v1/consult/allDoctor/list', {
      token: rootState.corsuser.user.token,
      pageNo: '1',
      pageSize: $C.pageSize,
    });
    if (!parseInt(res.status)) {
      commit('updateDoctor', res.result);
    } else {
      throw new Error(res.message);
    }
  },
  async loadMore({ commit, rootState, state }, index) {
    try {
      const res = await helper.POST($C.medSys + 'v1/consult/allDoctor/list', {
        token: rootState.corsuser.user.token,
        pageNo: index + 1,
        pageSize: $C.pageSize,
      });
      if (!parseInt(res.status)) {
        commit('updateDoctor', state.doctorList.concat(res.result));
      }
    } catch (e) {
      console.log(e);
    }
  },
  async refreshFriend({ commit, rootState }) {
    const res = await helper.POST($C.medSys + 'v1/consult/friendDoctor/list', {
      token: rootState.corsuser.user.token,
      pageNo: '1',
      pageSize: $C.pageSize,
    });
    if (!parseInt(res.status)) {
      commit('updateFriend', res.result);
    } else {
      throw new Error(res.message);
    }
  },
  async loadMoreFriend({ commit, rootState, state }, index) {
    try {
      const res = await helper.POST($C.medSys + 'v1/consult/friendDoctor/list', {
        token: rootState.corsuser.user.token,
        pageNo: index + 1,
        pageSize: $C.pageSize,
      });
      if (!parseInt(res.status)) {
        commit('updateFriend', state.friendList.concat(res.result));
      }
    } catch (e) {
      console.log(e);
    }
  },
};

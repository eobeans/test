FROM node:8.5
MAINTAINER relzhong@outlook.com

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
COPY . /usr/src/app
RUN npm config set loglevel warn
RUN npm install autod --save-dev
RUN npm run autod
RUN npm install
RUN npm run nuxt-generate

CMD [ "npm", "run", "start-prod" ]

# replace this with your application's default port
EXPOSE 7001

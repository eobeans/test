const path = require('path');
console.log('env -- PROXY_REWRITE : ' + process.env.PROXY_REWRITE);
console.log('API_HOST : ' + process.env.API_HOST);
console.log('Quasar themes: ' + process.env.THEME);
const pathBase = process.env.PROXY_REWRITE ? process.env.PROXY_REWRITE : '/cn/';
const theme = process.env.THEME ? process.env.THEME : 'mat'; // Quasar themes: "mat", "ios"
module.exports = {
  /*
  ** Headers of the page
  */
  env: {
    PROXY_REWRITE: pathBase,
    API_HOST: process.env.API_HOST,
    CSRF: true, // 开启csrf
  },
  generate: {
    dir: 'dist-mobile',
  },
  router: {
    mode: 'hash',
    base: './',
  },
  mode: 'spa',
  srcDir: path.join(__dirname, '../app/web-mobile'),
  head: {
    title: '睿图医联体',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'user-scalable=no,initial-scale=1,maximum-scale=1,minimum-scale=1,width=device-width' },
      { name: 'format-detection', content: 'telephone="no"' },
      { name: 'msapplication-tap-highlight', content: 'no' },
      { hid: 'description', name: 'description', content: '{{ description }}' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'favicon.ico' },
      { rel: 'stylesheet', href: 'https://cdn.bootcss.com/material-design-icons/3.0.1/iconfont/material-icons.min.css' },
    ],
    script: [
      { src: 'cordova.js', type: 'text/javascript' },
    ],
  },
  css: [
    `~/assets/themes/app.${theme}.styl`,
    '~/assets/css/common.css',
  ],
  /*
  ** Customize the progress bar color
  */
  loading: false,
  /*
  ** Build configuration
  */
  build: {
    // Extract CSS in a separated file
    extractCSS: true,
    publicPath: '/js/',
    /*
    ** Run ESLint on save
    */
    extend(config, ctx) {
      config.resolve.alias.quasar = `quasar-framework/dist/quasar.${theme}.esm`;
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
    vendor: [ 'ramda', 'js-base64', 'moment', 'axios', 'qs', '~/plugins/helper' ],
  },
  plugins: [ '~/plugins/quasar.js', '~/plugins/$F', '~/plugins/filter.js', '~/plugins/vuelidate', { src: '~/plugins/photoswipe', ssr: false }],
  // PWA manifest
  // https://github.com/nuxt-community/pwa-module
  manifest: {
    name: 'ylz-boilerplate-front',
    description: 'Mobile page for ylz-boilerplate.',
  },
};
